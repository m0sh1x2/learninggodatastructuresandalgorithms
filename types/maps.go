package main

import "fmt"

func main() {
	var test = map[int]string{
		3: "Test3",
		4: "Test4",
		5: "Test5",
	}

	fmt.Println(test)
	delete(test, 3)
	fmt.Println(test)

}
