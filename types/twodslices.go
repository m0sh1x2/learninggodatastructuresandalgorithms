package main

import "fmt"

func main() {
	var rows int
	var cols int
	rows = 7
	cols = 9

	var twodslices = make([][]int, rows)
	var i int
	for i = range twodslices {
		twodslices[i] = make([]int, cols)
	}

	fmt.Println(twodslices)
	twodslices = append(twodslices, make([]int, 3))

	fmt.Println(twodslices)

	var arr = []int{5, 6, 7, 8, 9}
	var slice1 = arr[2:4]
	fmt.Println(slice1)

}
