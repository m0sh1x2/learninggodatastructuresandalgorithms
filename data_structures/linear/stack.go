package main

import (
	"strconv"
)

type Element struct {
	elementValue int
}

func (element *Element) String() string {
	return strconv.Itoa(element.elementValue)
}

type Stack struct {
	elements     []*Element
	elementCount int
}

func (stack *Stack) New() {
	stack.elements = make([]*Element, 0)
}

func (stack *Stack) Push(element *Element) {
	stack.elements = append(stack.elements[:stack.elementCount], element)
	stack.elementCount = stack.elementCount + 1
}

func main() {
	var stack *Stack
	stack.New()

	var element1 *Element = &Element{3}
	var element2 *Element = &Element{2}
	var element3 *Element = &Element{10}
	stack.Push(element1)
	stack.Push(element2)
	stack.Push(element3)

	// fmt.Println(stack)
}
