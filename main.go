package main

import "fmt"

func sum(nums ...int) {
	fmt.Print(nums, " ")
	total := 0
	for _, num := range nums {
		total += num
	}
	fmt.Println(total)
}

func main() {

	// lists.MainList()

	// Tiples Example
	// var square int
	// var cube int

	// square, cube = tuples.GetTuples(3)
	// fmt.Println("Square ", square, "Cube ", cube)

	nums := []int{1, 2, 3, 5}
	sum(nums...)

}
