package main

import "fmt"

// IComposite interface

type IComposite interface {
	perform()
}

// Leaflet struct

type Leaflet struct {
	name string
}

// Leaflet class method perform

func (leaf *Leaflet) perform() {
	fmt.Println("Leaflet " + leaf.name)
}

// Branch Struct

type Branc struct {
	leafs    []Leaflet
	name     string
	branches []Branc
}

// Branch class method perform

func (branc *Branc) perform() {
	fmt.Println("Branch " + branc.name)
	{
		for _, leaf := range branc.leafs {
			leaf.perform()
		}
		for _, branch := range branc.branches {
			branch.perform()
		}
	}
}

// Branch class method add leaflet

func (branch *Branc) add(leaf Leaflet) {
	branch.leafs = append(branch.leafs, leaf)
}

// Branch class method addBranch branch

func (branch *Branc) addBranch(newBranch Branc) {
	branch.branches = append(branch.branches, newBranch)
}

// Branch Class method getLeaflets

func (branc *Branc) getLeaflets() []Leaflet {
	return branc.leafs
}

func main() {
	var branch = &Branc{name: "branch 1"}
	var leaf1 = &Leaflet{name: "leaf 1"}
	var leaf2 = &Leaflet{name: "leaf 2"}
	var branch2 = &Branc{name: "branch 2"}

	branch.add(*leaf1)
	branch.add(*leaf2)
	branch.addBranch(*branch2)
	branch.perform()
}
