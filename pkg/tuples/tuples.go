package tuples

func GetTuples(a int) (int, int) {
	return a * a, a * a * a
}
